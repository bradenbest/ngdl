## ngdl -- Downloader for newgrounds.

Using what I learned from creating [fadl](https://gitlab.com/bradenbest/fadl), I created a simple downloader for newgrounds.

This downloader will download anything as long as the page source contains an "alternate" source. Most movies and game on NG have "alternate" links.

## Usage

### Installation

    $ sudo ./install

### Usage

To download a movie, for example, Oney's infamous [Dragonzball P](http://www.newgrounds.com/portal/view/586125), you can do this:

    $ ngdl http://www.newgrounds.com/portal/view/586125
      or
    $ ngdl -p 586125

The -p stands for portal.

When you start it up, the script will automatically find the "alternate" link within the page's javascript, and proceed to download it.

You can also use list files.

    $ ngdl -l list-file (you can name the file anything you want)

Where `list-file` contains

    http://www.newgrounds.com/portal/view/586125
    http://www.newgrounds.com/portal/view/608431
    586943

And it will download all three. Notice that you can use straight ids in the file, too.

For fun, try `ngdl -p 1`, for the first ever portal submission.